__version__ = "1"

import torch.nn as nn
import torch.nn.functional as F


class MyModel(nn.Module):
    def __init__(self, idim=30, hdim=64):
        super(MyModel, self).__init__()
        self.conv1 = nn.Conv1d(idim, hdim, 3, padding=1)
        self.conv2 = nn.Conv1d(hdim, hdim, 3, padding=1)
        self.conv3 = nn.Conv1d(hdim, idim, 3, padding=1)
        self.lrelu = nn.LeakyReLU(negative_slope=0.2)

    def forward(self, x):
        x = self.lrelu(self.conv1(x))
        x = self.lrelu(self.conv2(x))
        x = self.lrelu(self.conv3(x))
        return x
