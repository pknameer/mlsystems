from torch import tensor
from torch.utils.data import Dataset


def encode_and_pad(l, max_len=30):
    def one_hot(l):
        return [[1 if it == i else 0 for i in range(6)] for it in l]

    d = {"+": 1, "-": 2, "*": 3, "/": 4}
    l = [d[i] if i in d else 5 for i in l]
    l = l + [0] * (max_len - len(l))
    l = one_hot(l)
    l = tensor(l).float()
    return l


class PPDataset(Dataset):
    def __init__(self, samples, max_len=30, use_cuda=False):
        self.max_len = max_len
        self.use_cuda = use_cuda
        self.data = [i for i in samples if len(i[0].split()) <= max_len]

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        item = dict()
        item["pre"], item["post"] = [i.split() for i in self.data[idx]]
        item["f_pre"], item["f_post"] = (
            encode_and_pad(item["pre"], self.max_len),
            encode_and_pad(item["post"], self.max_len),
        )
        if self.use_cuda:
            item["f_pre"], item["f_post"] = (
                item["f_pre"].cuda(),
                item["f_post"].cuda(),
            )
        return item


if __name__ == "__main__":
    import json
    from random import randint

    with open("train.json", "r") as fl:
        samples = json.load(fl)["samples"]
    dataset = PPDataset(samples)
    print(f"Length: {len(dataset)}")
    print(f"Sample item: {dataset[randint(0, len(dataset)-1)]}")
