import torch
import argparse
import logging
from dataset import encode_and_pad
from aiohttp import web

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser()
parser.add_argument("--model_path", type=str, required=True)
args = parser.parse_args()

model = torch.jit.load(args.model_path)
model.eval()


async def convert(request):
    pre = await request.json()
    pre = pre["pre"]
    decode = {0: "", 1: "+", 2: "-", 3: "*", 4: "/"}
    f_pre = pre.split()
    pre_values = [i for i in f_pre if i not in decode.values()]
    f_pre = encode_and_pad(f_pre).unsqueeze(dim=0)
    if next(model.parameters()).is_cuda:
        f_pre = f_pre.cuda()
    pred = torch.argmax(model(f_pre).squeeze(), dim=1).tolist()
    complete_prediction = len(pre_values) == pred.count(5)
    pre_values = pre_values + ["unk"] * (pred.count(5) - len(pre_values))
    post = [decode[i] if i in decode else pre_values.pop(0) for i in pred]
    return web.json_response(
        {"pre": pre, "post": " ".join(post).strip(), "complete": complete_prediction}
    )


if __name__ == "__main__":
    app = web.Application(debug=True)
    app.add_routes([web.post("/", convert)])
    web.run_app(app, port=8080, host="0.0.0.0")
