import torch
import json
import model as m_model
from math import ceil
from tqdm import tqdm
from dataset import PPDataset
from torch import cuda, optim, nn
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter


log_num = 10000

writer = SummaryWriter("runs/workshop_cuda_batch_size_1")
use_cuda = torch.cuda.is_available

with open("train.json", "r") as fl:
    samples = json.load(fl)["samples"]
split = ceil(0.9 * len(samples))

train_set, dev_set = (
    PPDataset(samples[:split], use_cuda=use_cuda),
    PPDataset(samples[split:], use_cuda=use_cuda),
)
train_loader = DataLoader(train_set, batch_size=1, shuffle=True)
dev_loader = DataLoader(dev_set, batch_size=1, shuffle=True)

model = m_model.MyModel()
if use_cuda:
    model.cuda()

criterion = nn.L1Loss()
optimizer = optim.Adam(model.parameters(), lr=0.001)


def is_em(out, target):
    return all([o.max(0)[1] == t.max(0)[1] for o, t in zip(out, target)])


training_loss = 0.0
for epoch in tqdm(range(100), desc="Epoch"):
    for i, data in enumerate(tqdm(train_loader, desc="Train", leave=False)):
        optimizer.zero_grad()
        inp = data["f_pre"]
        t_len = len(data["post"])
        out = model(inp)
        target = data["f_post"][:t_len, :]
        loss = criterion(out[:t_len, :], target)
        loss.backward()
        optimizer.step()

        training_loss += loss.item()
        if (i + 1) % log_num == 0:
            dev_loss = 0.0
            em = 0
            for j, dev in enumerate(tqdm(dev_loader, desc="Dev", leave=False)):
                inp = dev["f_pre"]
                t_len = len(dev["post"])
                out = model(inp)[:t_len, :]
                target = dev["f_post"][:t_len, :]
                loss = criterion(out[:t_len, :], target)
                em += sum([is_em(out, target) for out, target in zip(out, target)])
                dev_loss += loss.item()
            writer.add_scalar(
                "train_loss", training_loss / log_num, epoch * len(train_set) + i
            )
            writer.add_scalar(
                "dev_loss",
                dev_loss / len(dev_set),
                epoch * len(dev_set) + (i + 1) / log_num,
            )
            writer.add_scalar(
                "em", em / len(dev_set), epoch * len(dev_set) + (i + 1) / log_num
            )
            training_loss = 0.0
    # save model
    model.zero_grad()
    traced_model = torch.jit.trace(model, (inp,))
    traced_model.save("models/model_" + m_model.__version__ + f"_epoch_{epoch}")

model.zero_grad()
traced_model = torch.jit.trace(model, (inp,))
traced_model.save("models/model_" + m_model.__version__ + "_final")
