import random


class Op:
    def __init__(self, kind, left, right):
        self.kind = kind
        self.left = left
        self.right = right

    def pre(self):
        return [self.kind, *self.left.pre(), *self.right.pre()]

    def post(self):
        return [*self.left.post(), *self.right.post(), self.kind]


class Num:
    def __init__(self, digits, n=None):
        self.digits = digits
        if n is None:
            n = str(random.randint(0, int("9" * max(1, digits))))
        self.n = n

    def pre(self):
        return [self.n]

    def post(self):
        return [self.n]


def generate(digits, tree_depth, depth=0):
    if depth >= tree_depth or random.choice([True, False]):
        return Num(digits=digits)
    return Op(
        random.choice("+-/*"),
        generate(digits, tree_depth, depth + 1),
        generate(digits, tree_depth, depth + 1),
    )


if __name__ == "__main__":
    from tqdm import tqdm
    import json

    training = set()
    testing = set()
    maxdigits = 4
    maxdepth = 8
    total = 100_000
    feather = "7"  # youtube feather
    with tqdm(total=total, desc="Train") as tr_pbar, tqdm(
        total=total, desc="Test"
    ) as ts_pbar:
        while len(training) < total or len(testing) < total:
            eqn = generate(maxdigits, maxdepth)
            pre = " ".join(eqn.pre())
            pbar, target = (
                (tr_pbar, training) if feather not in pre else (ts_pbar, testing)
            )
            key = (pre, " ".join(eqn.post()))
            if key not in training and key not in testing and len(target) < total:
                pbar.update(1)
                target.add(key)
    with open("train.json", "w") as fl:
        json.dump({"samples": [i for i in training]}, fl, indent=2)
    with open("test.json", "w") as fl:
        json.dump({"samples": [i for i in testing]}, fl, indent=2)
